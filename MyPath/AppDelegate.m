//
//  AppDelegate.m
//  MyPath
//
//  Created by Abraham Tesfamariam on 8/7/17.
//  Copyright © 2017 RJT Compuquest. All rights reserved.
//

#import "AppDelegate.h"
@import Firebase;
@import GoogleMaps;
@import UserNotifications;

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [GMSServices provideAPIKey:@"AIzaSyB2J0JBJ8IFUMtenPiuTU9HapIFkxMz4Cw"];
    [FIRApp configure];
    
    // HomeVC becomes root view controller if app starts up and user is already logged in via email or fb
    if (FIRAuth.auth.currentUser != nil){
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UINavigationController *vc = [storyboard instantiateViewControllerWithIdentifier:@"navController"];
        [self.window setRootViewController:vc];
    }
    
    [UNUserNotificationCenter.currentNotificationCenter requestAuthorizationWithOptions:UNAuthorizationOptionAlert|UNAuthorizationOptionSound completionHandler:^(BOOL granted, NSError * _Nullable error) {
        if(!granted) {
            NSLog(@"Notifications not authorized by user.");
        }
    }];
    
    return YES;
}

- (void) scheduleNotifications {
    UNTimeIntervalNotificationTrigger *trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:60 repeats:YES];
    
    UNMutableNotificationContent * content;
    [content setTitle:@"MyPath"];
    [content setBody:@"Reminder to workout with MainStreetPath"];
    
    UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:@"MyPathRequest" content:content trigger:trigger];
    [UNUserNotificationCenter.currentNotificationCenter removeAllPendingNotificationRequests];
    [UNUserNotificationCenter.currentNotificationCenter addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
        NSLog(@"%@", error.localizedDescription);
    }];
    
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
