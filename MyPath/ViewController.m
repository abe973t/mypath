//
//  ViewController.m
//  MyPath
//
//  Created by Abraham Tesfamariam on 8/7/17.
//  Copyright © 2017 RJT Compuquest. All rights reserved.
//

#import "ViewController.h"
#import "Model.h"
#import "MyPath-Swift.h"
@import FirebaseAuth;
@import FirebaseDatabase;

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UITextField *firstNameTxtField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTxtField;
@property (weak, nonatomic) IBOutlet ShakingTextField *signUpEmailTxtField;
@property (weak, nonatomic) IBOutlet UITextField *phoneTxtField;
@property (weak, nonatomic) IBOutlet ShakingTextField *signUpPassTxtField;
@property (weak, nonatomic) IBOutlet ShakingTextField *signUpReenterPassTxtField;
@property (weak, nonatomic) IBOutlet UIView *signUpView;

@property (weak, nonatomic) IBOutlet ShakingTextField *emailTxtField;
@property (weak, nonatomic) IBOutlet ShakingTextField *passTxtField;
@property (weak, nonatomic) IBOutlet AnimatedImageView *emailTxtBG;
@property (weak, nonatomic) IBOutlet AnimatedImageView *passTxtBG;
@property (weak, nonatomic) IBOutlet UIImageView *btnImg;

@property UITapGestureRecognizer *tapGestureRecognizer;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    // change placeholder text to white
    UIColor *color = [UIColor whiteColor];
    self.emailTxtField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email:" attributes:@{NSForegroundColorAttributeName: color}];
    self.passTxtField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password:" attributes:@{NSForegroundColorAttributeName: color}];
    
    self.tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addPulse)];
    self.tapGestureRecognizer.numberOfTapsRequired = 1;
    [self.btnImg addGestureRecognizer:self.tapGestureRecognizer];
    
    self.emailTxtField.text = @"abraham0657@gmail.com";
    self.passTxtField.text = @"Abraham";
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) addPulse {
    Pulse *pulse = [[Pulse alloc] initWithNumberOfPulses:1 radius:530 position:self.btnImg.center];
    pulse.animationDuration = 0.8;
    pulse.backgroundColor = [[UIColor blueColor] CGColor];
    
    [self.view.layer insertSublayer:pulse below:self.btnImg.layer];
}

- (IBAction)hideSignUp:(UIButton *)sender {
    [UIView animateWithDuration:0.5 animations:^{
        self.signUpView.frame = CGRectMake(33, 133, 0, 0);
    }];
    
    self.signUpView.hidden = YES;
}

- (IBAction)showSignUp:(UIButton *)sender {
    if (self.signUpView.isHidden) {
        self.signUpView.hidden = NO;
        self.signUpView.frame = CGRectMake(350, 133, 350, 400);
        
        [UIView animateWithDuration:0.25 animations:^{
            self.signUpView.frame = CGRectMake(33, 133, 350, 400);
        }];
    }
}

- (IBAction)signUpAction:(UIButton *)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Alert" message:@"" preferredStyle: UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction: ok];
    
    if ([self.firstNameTxtField.text isEqual: @""] || [self.lastNameTxtField.text isEqual: @""] || [self.signUpEmailTxtField.text isEqual: @""] || [self.phoneTxtField.text isEqual: @""] || [self.signUpPassTxtField.text isEqual: @""] || [self.signUpReenterPassTxtField.text isEqual: @""]) {
        [alert setMessage:@"Please fill out all fields."];
        [self presentViewController:alert animated:YES completion:nil];
    } else if ([self isValidEmail:self.signUpEmailTxtField.text]) {
        [alert setMessage:@"Email is in invalid format."];
        [self presentViewController:alert animated:YES completion:nil];
    } else if (![self.signUpPassTxtField.text isEqualToString:self.signUpReenterPassTxtField.text]) {
        [alert setMessage:@"Passwords do not match."];
        [self presentViewController:alert animated:YES completion:nil];
    } else {
        self.ref = [[FIRDatabase database] reference];
        
        [[FIRAuth auth] createUserWithEmail:self.signUpEmailTxtField.text password:self.signUpReenterPassTxtField.text completion:^(FIRUser * _Nullable user, NSError * _Nullable error) {
            if (error == nil) {
                NSLog(@"%@ has successfully created a Firebase account!", self.firstNameTxtField.text);
                FIRUserProfileChangeRequest *changeRequest = [user profileChangeRequest];
                changeRequest.displayName = [[self.firstNameTxtField.text stringByAppendingString:@" "] stringByAppendingString:self.lastNameTxtField.text];
                [changeRequest commitChangesWithCompletion:nil];
                
                [[[[self.ref child:@"users"] child:user.uid] child:@"Username"] setValue:self.signUpEmailTxtField.text];
                [[[[self.ref child:@"users"] child:user.uid] child:@"Full name"] setValue:[[self.firstNameTxtField.text stringByAppendingString:@" "] stringByAppendingString:self.lastNameTxtField.text]];
                [[[[self.ref child:@"users"] child:user.uid] child:@"Phone"] setValue:self.phoneTxtField.text];
                
                [alert setMessage:@"Sign up succesful!"];
                [self presentViewController:alert animated:YES completion:^{
                    self.signUpView.hidden = YES;
                }];
            } else {
                [alert setMessage:error.localizedDescription];
                [self presentViewController:alert animated:YES completion:^{
                    self.signUpView.hidden = YES;
                }];
            }
        }];
    }
}

- (IBAction)signInAction:(UIButton *)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Alert" message:@"" preferredStyle: UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction: ok];

    // TODO: look for new email validation function!
    if ([self isValidEmail:self.emailTxtField.text]) {
        self.emailTxtField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email is in invalid format" attributes:@{NSForegroundColorAttributeName: [UIColor redColor]}];
        [self.emailTxtField setText:@""];
        [[self emailTxtBG] shake];
    } else if ([self.emailTxtField.text isEqualToString:@""]) {
        self.emailTxtField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Please provide email" attributes:@{NSForegroundColorAttributeName: [UIColor redColor]}];
        [self.emailTxtField setText:@""];
        [[self emailTxtBG] shake];
    } else if ([self.passTxtField.text isEqualToString:@""]) {
        self.passTxtField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Please provide password" attributes:@{NSForegroundColorAttributeName: [UIColor redColor]}];
        [self.passTxtField setText:@""];
        [[self passTxtBG] shake];
    } else {
        [[FIRAuth auth] signInWithEmail:self.emailTxtField.text password:self.passTxtField.text completion:^(FIRUser * _Nullable user, NSError * _Nullable error) {
            if (error == nil) {
                NSLog(@"%@ is signed in.", user.uid);
                
                UIViewController *dVC = [[self storyboard] instantiateViewControllerWithIdentifier:@"navController"];
                [self presentViewController:dVC animated:YES completion:^{
                    // try to include twmessagebar notification here
                }];
            } else {
                [alert setMessage:error.localizedDescription];
                [self presentViewController:alert animated:YES completion:nil];
            }
        }];
    }
}

- (BOOL)isValidEmail:(NSString *)testStr {
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}";
    
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    return [emailTest evaluateWithObject:emailRegEx];
}

@end
