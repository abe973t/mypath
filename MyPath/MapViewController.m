//
//  MapViewController.m
//  MyPath
//
//  Created by Abraham Tesfamariam on 8/8/17.
//  Copyright © 2017 RJT Compuquest. All rights reserved.
//

#import "MapViewController.h"
#import "Model.h"
#import <QuartzCore/QuartzCore.h>
#import "RunsViewController.h"
@import GoogleMaps;
@import CoreLocation;
@import FirebaseAuth;
@import CoreBluetooth;

#define HEARTRATE @"7821C91E-A551-4907-A5E0-F6CB64AC0A4B"
#define STEPSCOUNT @"EE6134CF-F907-45CD-B259-2AB681CA6B32"
#define TEMPERATURE @"2BCE8CF5-F03E-4EB2-BB35-77C87AC5F1A4"

@interface MapViewController () <CBCentralManagerDelegate, CBPeripheralDelegate, RunsListDelegate>

@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@property (weak, nonatomic) IBOutlet UIButton *startBtn;
@property (weak, nonatomic) IBOutlet UIButton *stopBtn;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *settingsBtn;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *historyBtn;

@property CLLocationManager *locationmanager;
@property CLLocation *currentLocation;
@property GMSMarker *marker;

@property GMSPolyline *polyline;        // draws a physical line on the map
@property GMSMutablePath *gmsPath;      // array of CLLocationCoordinate2D
@property NSMutableDictionary *path;    // custom path created by user
@property NSMutableArray *markers;
@property NSString *timeRunUpdated;
@property NSString *startTime;
@property CFTimeInterval startTimer;
@property NSMutableArray *heartRates;
@property NSMutableArray *coordinates;

@property BOOL didStartRunning;
@property Model* modelObj;

@property (nonatomic, strong) CBCentralManager *manager;
@property(nonatomic, strong) CBPeripheral *peripheral;
@property(nonatomic, strong) CBCharacteristic *heartRate;
@property(nonatomic, strong) CBCharacteristic *stepsCount;
@property(nonatomic, strong) CBCharacteristic *temperatureStat;

@end

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // initialize objects
    self.locationmanager = [[CLLocationManager alloc] init];
    self.polyline = [[GMSPolyline alloc] init];
    self.path = [[NSMutableDictionary alloc] init];
    self.gmsPath = [[GMSMutablePath alloc] init];
    self.markers = [[NSMutableArray alloc] init];
    self.modelObj = [[Model alloc] init];
    self.manager = [[CBCentralManager alloc] initWithDelegate:self queue:nil options:nil];
    self.heartRates = [NSMutableArray new];

    // set up location manager
    self.locationmanager.delegate = self;
    [self.locationmanager requestWhenInUseAuthorization];
    [self.locationmanager requestLocation];
    self.locationmanager.desiredAccuracy = kCLLocationAccuracyBest;
    
    [self setTitle:@"My Path 🏃🏾"];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    
    // customize UI for Night Mode
    BOOL nightModeEnabled = [[NSUserDefaults standardUserDefaults] boolForKey:@"NightModeEnabled"];
    UINavigationBar *bar = [self.navigationController navigationBar];
    
    if(nightModeEnabled) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [bar setBarTintColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:1.0]];
            [self.settingsBtn setTintColor:[UIColor whiteColor]];
            [self.historyBtn setTintColor:[UIColor whiteColor]];
        });
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [bar setBarTintColor:[UIColor colorWithRed:0 green:93.0/255.0 blue:160.0/255.0 alpha:1.0]];
            [self.settingsBtn setTintColor:[UIColor blackColor]];
            [self.historyBtn setTintColor:[UIColor blackColor]];
        });
    }
    
    // draw old path if data was passed in
    if(self.coordinates != nil) {
        [self drawOldPath];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"runsView"]) {
        RunsViewController *vc = [segue destinationViewController];
        vc.delegate = self;
    }
}

- (void) drawOldPath {
    [self.mapView clear];
    [self.gmsPath removeAllCoordinates];
    
    for (NSValue *v in self.coordinates) {
        CLLocationCoordinate2D coord;
        [v getValue:&coord];
        NSLog(@"%f", coord.longitude);
        
        [self.gmsPath addCoordinate:coord];
    }
    
    [self addRouteToPath];
}

-(void)addItemToVC:(RunsViewController *)controller item:(NSMutableArray *)item {
    self.coordinates = [NSMutableArray new];
    self.coordinates = item;
}

- (void) locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    self.currentLocation = locations.lastObject;
    GMSCameraPosition *cameraPosition = [GMSCameraPosition cameraWithTarget:self.currentLocation.coordinate zoom:20];
    self.mapView.camera = cameraPosition;
    
    if (self.didStartRunning) {
        // start drawing path
        [self addRouteToPath:self.currentLocation.coordinate];

        // store coordinates in dictionary with time location updated as key
        [self.path setValue:[NSNumber numberWithDouble: self.currentLocation.coordinate.latitude] forKey:@"lat"];
        [self.path setValue:[NSNumber numberWithDouble: self.currentLocation.coordinate.longitude] forKey:@"lng"];
        [self.modelObj writeToJSON:self.path];
    }
}

- (IBAction)startHikingTrail:(UIButton *)sender {
    [self.locationmanager startUpdatingLocation];
    [self.mapView clear];
    self.startBtn.enabled = NO;
    self.stopBtn.enabled = YES;
    self.didStartRunning = YES;
    
    [self.modelObj writeToJSON:self.path];
    
    // mark beginning of trail
    [self createMarkerAt:self.currentLocation.coordinate title:@"Start" withImage:[UIImage imageNamed:@"starting_point"]];
    
    NSDate *now = [NSDate date];
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"MM-dd-yyyy hh-mm-ss"];
    self.startTime = [outputFormatter stringFromDate:now];  // title for json file
    
    [outputFormatter setDateFormat:@"hh-mm-ss"];
    self.timeRunUpdated = [outputFormatter stringFromDate:now];
    [self.path setValue:self.timeRunUpdated forKey:@"time"];
    self.startTimer = CACurrentMediaTime();

    // store path dictionary in Documents with date & time
    [self.modelObj startRunLogWithTime:self.startTime];
    
    if(!self.peripheral.name) {
        NSLog(@"Could not find heart-rate sensor!");
    }
}

- (IBAction)stopHikingTrail:(UIButton *)sender {
    [self.locationmanager stopUpdatingLocation];
    self.startBtn.enabled = YES;
    self.stopBtn.enabled = NO;
    self.didStartRunning = NO;
    
    // mark ending of trail
    [self createMarkerAt:self.currentLocation.coordinate title:@"End" withImage:[UIImage imageNamed:@"_flag_ending"]];
    [self focusMapToShowAllMarkers];
    
    NSDate *now = [NSDate date];
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"hh-mm-ss"];
    self.timeRunUpdated = [outputFormatter stringFromDate:now];
    [self.path setValue:self.timeRunUpdated forKey:@"time"];
    CFTimeInterval elapsedTime = CACurrentMediaTime() - self.startTimer;

    // elapsedTime to hh:mm:ss format
    NSNumber *theDouble = [NSNumber numberWithDouble:elapsedTime];
    
    int inputSeconds = [theDouble intValue];
    int hours =  inputSeconds / 3600;
    int minutes = ( inputSeconds - hours * 3600 ) / 60;
    int seconds = inputSeconds - hours * 3600 - minutes * 60;
    NSString *theTime = [NSString stringWithFormat:@"%.2d:%.2d:%.2d", hours, minutes, seconds];
    [self.path setValue:theTime forKey:@"timeElapsed"];
    
    if(self.peripheral.name) {
        // stores steps value if peripheral is discovered
        [self.path setValue:[[NSString alloc] initWithData:self.stepsCount.value encoding:NSUTF8StringEncoding] forKey:@"steps"];
        
        // calculate heart rate avg
        int sum = 0;
        double avg = 0;
        for (NSNumber *n in self.heartRates) {
            sum += [n intValue];
        }
        
        avg = sum / self.heartRates.count;
        NSNumber *tempAvg = [NSNumber numberWithDouble:avg];
        
        [self.path setValue:[tempAvg stringValue] forKey:@"avgHeartRate"];
    }
    
    [self.modelObj stopRun:self.path];
    [self.modelObj saveRunToFireBase];
}

- (void)focusMapToShowAllMarkers {
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
    
    for (GMSMarker *marker in self.markers)
        bounds = [bounds includingCoordinate:marker.position];
    
    [self.mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:30.0f]];
}
                                         
- (void)addRouteToPath:(CLLocationCoordinate2D)coord {
    [self.gmsPath addCoordinate:coord];
    [self.polyline setPath:self.gmsPath];
    [self.polyline setStrokeColor:[UIColor redColor]];
    [self.polyline setStrokeWidth:5];
    [self.polyline setGeodesic:YES];
    
    GMSCameraPosition *cameraPosition = [GMSCameraPosition cameraWithTarget:coord zoom:20];
    self.mapView.camera = cameraPosition;
    
    [CATransaction begin];
    [CATransaction setAnimationDuration:1];
    [self.polyline setMap:self.mapView];
    [CATransaction commit];
}

- (void)addRouteToPath {
    [self.polyline setPath:self.gmsPath];
    [self.polyline setStrokeColor:[UIColor blackColor]];
    [self.polyline setStrokeWidth:2.5];
    [self.polyline setGeodesic:YES];
    
    // create markers at beginning and end of path
    [self createMarkerAt:[self.gmsPath coordinateAtIndex:0] title:@"Started" withImage:[UIImage imageNamed:@"starting_point"]];
    [self createMarkerAt:[self.gmsPath coordinateAtIndex:[self.gmsPath count]-1] title:@"Finished" withImage:[UIImage imageNamed:@"_flag_ending"]];
    
    // positions camera to beginning of path
    GMSCameraPosition *cameraPosition = [GMSCameraPosition cameraWithTarget:[self.gmsPath coordinateAtIndex:0] zoom:18];
    self.mapView.camera = cameraPosition;
    
    [CATransaction begin];
    [CATransaction setAnimationDuration:1];
    [self.polyline setMap:self.mapView];
    [CATransaction commit];
}

- (void) locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    NSLog(@"%@", error.localizedDescription);
}

- (void) createMarkerAt:(CLLocationCoordinate2D)coordinate title:(NSString *)title withImage:(UIImage *)image {
    GMSMarker *marker = [GMSMarker markerWithPosition:coordinate];
    marker.title = title;
    marker.icon = image;
    marker.map = self.mapView;
    GMSCameraPosition *cameraPosition = [GMSCameraPosition cameraWithTarget:marker.position zoom:20];
    self.mapView.camera = cameraPosition;
    
    [self.markers addObject:marker];
}

- (IBAction)viewHistoryBtnAction:(UIBarButtonItem *)sender {
    [[self modelObj] loadRuns];
}
         
// MARK: - Bluetooth functionality
         
 -(void)centralManagerDidUpdateState:(CBCentralManager *)central {
     if (central.state == CBManagerStatePoweredOn) {
         [self.manager scanForPeripheralsWithServices:nil options:nil];
     }
 }
 
 -(void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary<NSString *,id> *)advertisementData RSSI:(NSNumber *)RSSI {
     
     NSLog(@"%@", peripheral.name);
     
     if ([peripheral.name isEqualToString:@"Abraham’s MacBook Pro"]) {
         self.peripheral = peripheral;
         [self.manager connectPeripheral:peripheral options:nil];
         [self.manager stopScan];
         NSLog(@"Connected");
     }
 }

 
 -(void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral {
     peripheral.delegate = self;
     [peripheral discoverServices:nil];
 }
 
 
 -(void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error {
     for (CBService *service in peripheral.services) {
         [peripheral discoverCharacteristics:nil forService:service];
     }
 }
         
 -(void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error {
     
     for (CBCharacteristic *characteristic in service.characteristics) {
         
         if ([characteristic.UUID.UUIDString isEqualToString: HEARTRATE]) {
             self.heartRate = characteristic;
             [peripheral setNotifyValue:YES forCharacteristic:self.heartRate];
         }
         
         if ([characteristic.UUID.UUIDString isEqualToString:STEPSCOUNT]) {
             self.stepsCount= characteristic;
             [peripheral readValueForCharacteristic:self.stepsCount];
         }
         
         if ([characteristic.UUID.UUIDString isEqualToString:TEMPERATURE]) {
             self.temperatureStat = characteristic;
             [peripheral readValueForCharacteristic:self.stepsCount];
         }
     }
 }
         
- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    if (characteristic == self.heartRate) {
        NSString *v = [[NSString alloc] initWithData:characteristic.value encoding:NSUTF8StringEncoding];
        
        if(self.didStartRunning) {
            int inn = [v intValue];
            NSNumber *i = [[NSNumber alloc] initWithInt:inn];

            [self.heartRates addObject: i];
        }
        
        NSLog(@"Heart Rate: %@", v);
    }
    
    if (characteristic == self.stepsCount) {
        NSString *v = [[NSString alloc] initWithData:characteristic.value encoding:NSUTF8StringEncoding];
        NSLog(@"Steps Count: %@", v);
    }
    
    if (characteristic == self.temperatureStat) {
        NSString *v = [[NSString alloc] initWithData:characteristic.value encoding:NSUTF8StringEncoding];
        NSLog(@"Temperature: %@", v);
    }
}
         
- (IBAction) getSteps: (UIButton *)sender {
    [self.peripheral readValueForCharacteristic:self.stepsCount];
 }
 
 - (IBAction) getTemperature: (UIButton *)sender {
     [self.peripheral readValueForCharacteristic:self.temperatureStat];
 }

@end
