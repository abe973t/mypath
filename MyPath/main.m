//
//  main.m
//  MyPath
//
//  Created by Abraham Tesfamariam on 8/7/17.
//  Copyright © 2017 RJT Compuquest. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
