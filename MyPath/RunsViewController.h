//
//  RunsViewController.h
//  MyPath
//
//  Created by Abraham Tesfamariam on 8/11/17.
//  Copyright © 2017 RJT Compuquest. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RunsViewController;
@protocol RunsListDelegate <NSObject>
- (void)addItemToVC:(RunsViewController *)controller item:(NSMutableArray *)item;
@end

@interface RunsViewController : UIViewController

@property (weak, nonatomic) id <RunsListDelegate> delegate;

@end
