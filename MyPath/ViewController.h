//
//  ViewController.h
//  MyPath
//
//  Created by Abraham Tesfamariam on 8/7/17.
//  Copyright © 2017 RJT Compuquest. All rights reserved.
//

#import <UIKit/UIKit.h>
@import FirebaseDatabase;
@class ShakingTextField;

@interface ViewController : UIViewController

@property (strong, nonatomic) FIRDatabaseReference *ref;

-(BOOL)isValidEmail:(NSString *)testStr;
- (IBAction)hideSignUp:(UIButton *)sender;
- (IBAction)showSignUp:(UIButton *)sender;

@end

