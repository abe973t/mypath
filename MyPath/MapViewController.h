//
//  MapViewController.h
//  MyPath
//
//  Created by Abraham Tesfamariam on 8/8/17.
//  Copyright © 2017 RJT Compuquest. All rights reserved.
//

#import <UIKit/UIKit.h>
@import CoreLocation;

@interface MapViewController : UIViewController<CLLocationManagerDelegate>

- (void) drawOldPath;

@end
