//
//  RunsTableViewCell.m
//  MyPath
//
//  Created by Abraham Tesfamariam on 8/14/17.
//  Copyright © 2017 RJT Compuquest. All rights reserved.
//

#import "RunsTableViewCell.h"

@implementation RunsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
