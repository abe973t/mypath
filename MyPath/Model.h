//
//  Model.h
//  MyPath
//
//  Created by Abraham Tesfamariam on 8/7/17.
//  Copyright © 2017 RJT Compuquest. All rights reserved.
//

#import <Foundation/Foundation.h>
@import FirebaseAuth;
@import FirebaseDatabase;
@import FirebaseStorage;

@interface Model : NSObject

@property NSFileHandle *filehandle;
@property NSString *jsonFilePath;
@property (strong, nonatomic) FIRDatabaseReference *ref;
@property (strong, nonatomic) FIRStorage *storage;

- (void) startRunLogWithTime:(NSString *)startTime;
- (void) writeToJSON:(NSMutableDictionary *)path;
- (void) stopRun:(NSMutableDictionary *)path;
- (void) saveRunToFireBase;
- (NSArray *) loadRuns;

@end
