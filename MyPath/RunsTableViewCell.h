//
//  RunsTableViewCell.h
//  MyPath
//
//  Created by Abraham Tesfamariam on 8/14/17.
//  Copyright © 2017 RJT Compuquest. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RunsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *timeElapsedLbl;

@property (weak, nonatomic) IBOutlet UILabel *numStepsLbl;
@property (weak, nonatomic) IBOutlet UILabel *avgTempLbl;

@property (weak, nonatomic) IBOutlet UILabel *timeLbl;

@end
