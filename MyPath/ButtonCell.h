//
//  ButtonCell.h
//  MyPath
//
//  Created by Abraham Tesfamariam on 8/31/17.
//  Copyright © 2017 RJT Compuquest. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ButtonCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *settingsBtn;

@end
