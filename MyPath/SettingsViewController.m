//
//  SettingsViewController.m
//  MyPath
//
//  Created by Abraham Tesfamariam on 8/11/17.
//  Copyright © 2017 RJT Compuquest. All rights reserved.
//

#import "SettingsViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "ToggableSettingsCell.h"
#import "ButtonCell.h"
#import "AppDelegate.h"
@import FirebaseAuth;

@interface SettingsViewController () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property NSArray *settings;
@property NSArray *buttons;

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.settings = [NSArray arrayWithObjects: @"Night Mode", @"Notifications", @"Offline Mode", nil];
    self.buttons = [NSArray arrayWithObjects: @"Delete local data", @"Sign Out", nil];
    
    [self.tblView setBackgroundColor: [UIColor clearColor]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return self.settings.count;
    } else if (section == 1) {
        return self.buttons.count;
    } else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.section == 0) {
        [self.tblView setRowHeight:70];
        
        // set up cell
        ToggableSettingsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"toggableCell"];
        cell.settingNameLbl.text = [self.settings objectAtIndex:indexPath.row];
        cell.settingsSwitch.tag = indexPath.row;
        [cell.settingsSwitch addTarget:self action:@selector(toggleSettings:) forControlEvents:UIControlEventValueChanged];
        
        // set night mode switch
        if(indexPath.row == 0) {
            BOOL nightMode = [[NSUserDefaults standardUserDefaults] boolForKey:@"NightModeEnabled"];
                
            if (nightMode) {
                [cell.settingsSwitch setOn:YES];
                NSLog(@"Should be on");
            } else {
                [cell.settingsSwitch setOn:NO];
                NSLog(@"Should NOT be on");
            }
        }
        
        return cell;
    } else {
        [self.tblView setRowHeight:50];

        ButtonCell *cell = [tableView dequeueReusableCellWithIdentifier:@"buttonCell"];
        
        [cell.settingsBtn setTitle:[self.buttons objectAtIndex:indexPath.row] forState:UIControlStateNormal];
        [cell.settingsBtn setTag:indexPath.row];
        [cell.settingsBtn addTarget:self action:@selector(settingsButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
    }
}

- (IBAction)settingsButtonClicked:(UIButton *)sender {
    switch (sender.tag) {
        case 0:
            [self clearCache];
            break;
        case 1:
            [self signOutAction];
            break;
        default:
            break;
    }
}

- (IBAction)toggleSettings:(UISwitch *)sender {
    UINavigationBar *bar = [self.navigationController navigationBar];
    
    switch (sender.tag) {
        case 0:
            if (sender.isOn) {
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"NightModeEnabled"];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [bar setBarTintColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:1.0]];
                });
            } else {
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"NightModeEnabled"];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [bar setBarTintColor:[UIColor colorWithRed:0 green:93.0/255.0 blue:160.0/255.0 alpha:1.0]];
                });
            }
            break;
        case 1:
            [(AppDelegate *) [[UIApplication sharedApplication] delegate] scheduleNotifications];
            break;
        case 2:
            // call offlineMode method from Model
        default:
            break;
    }
}


- (void)clearCache {
    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *docPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject];
    NSString *user = [[[FIRAuth auth] currentUser] uid];
    NSString *directory = [[NSString alloc] initWithFormat:@"%@/%@", docPath, user];
    NSError *error = nil;
    
    for (NSString *file in [fm contentsOfDirectoryAtPath:directory error:&error]) {
        BOOL success = [fm removeItemAtPath:[NSString stringWithFormat:@"%@/%@", directory, file] error:&error];
        if (!success || error) {
            NSLog(@"deleting files failed.");
        } else {
            NSLog(@"FILES SUCCESSFULLY DELETED!!");
        }
    }
}


- (void)signOutAction {
    [[FIRAuth auth] signOut:nil];
    
    UIViewController *dVC = [[self storyboard] instantiateViewControllerWithIdentifier:@"ViewController"];
    [self presentViewController:dVC animated:YES completion:^{
        // try to include twmessagebar notification here
    }];
}

@end
