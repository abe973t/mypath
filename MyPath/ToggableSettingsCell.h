//
//  ToggableSettingsCell.h
//  MyPath
//
//  Created by Abraham Tesfamariam on 8/19/17.
//  Copyright © 2017 RJT Compuquest. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ToggableSettingsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *settingNameLbl;
@property (weak, nonatomic) IBOutlet UISwitch *settingsSwitch;


@end
