 //
//  RunsViewController.m
//  MyPath
//
//  Created by Abraham Tesfamariam on 8/11/17.
//  Copyright © 2017 RJT Compuquest. All rights reserved.
//

#import "RunsViewController.h"
#import "Model.h"
#import "RunsTableViewCell.h"
#import "MapViewController.h"
@import CoreLocation;

@interface RunsViewController () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tblView;

@property Model *modelObj;
@property NSArray *runs;

@end

@interface NSDictionary(JSONCategories)
+(NSDictionary*)dictionaryWithContentsOfJSONURLString:(NSString*)urlAddress;
-(NSData*)toJSON;
@end

@implementation NSDictionary(JSONCategories)

+ (NSDictionary*)dictionaryWithContentsOfJSONURLString: (NSString*)urlAddress {
    NSData* data = [NSData dataWithContentsOfURL:
                    [NSURL URLWithString: urlAddress] ];
    __autoreleasing NSError* error = nil;
    id result = [NSJSONSerialization JSONObjectWithData:data
                                                options:kNilOptions error:&error];
    if (error != nil) return nil;
    return result;
}

- (NSData*)toJSON {
    NSError* error = nil;
    id result = [NSJSONSerialization dataWithJSONObject:self options:kNilOptions error:&error];
    if (error != nil) return nil;
    return result;
}

@end

@implementation RunsViewController
@synthesize delegate;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.modelObj = [Model new];
    self.runs = [self.modelObj loadRuns];
    [self.tblView setBackgroundColor:[UIColor clearColor]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    
    // customize UI for Night Mode
    BOOL nightModeEnabled = [[NSUserDefaults standardUserDefaults] boolForKey:@"NightModeEnabled"];
    UINavigationBar *bar = [self.navigationController navigationBar];
    
    if(nightModeEnabled) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [bar setBarTintColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:1.0]];
        });
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [bar setBarTintColor:[UIColor colorWithRed:0 green:93.0/255.0 blue:160.0/255.0 alpha:1.0]];
        });
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.runs.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    RunsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"runsCell"];
    
    // if json file is local: retrieve data, parse, etc
    NSData *runData = [[NSData alloc] initWithContentsOfURL:[[NSURL alloc] initFileURLWithPath:[self.runs objectAtIndex:indexPath.row]]];
    NSArray *jsonData = [NSJSONSerialization JSONObjectWithData:runData options:NSJSONReadingAllowFragments error:nil];
    NSDictionary *temp = [jsonData lastObject];
    NSString *date = [[[[[self.runs objectAtIndex:indexPath.row] pathComponents] lastObject] componentsSeparatedByString:@" "] firstObject];
    NSString *time = [[temp objectForKey:@"time"] stringByReplacingOccurrencesOfString:@"-" withString:@":"];
    
    cell.timeElapsedLbl.text = [temp objectForKey:@"timeElapsed"];
    cell.numStepsLbl.text = [temp objectForKey:@"steps"];
    cell.avgTempLbl.text = [temp objectForKey:@"avgHeartRate"];
    cell.timeLbl.text = [[date stringByAppendingString:@"  "] stringByAppendingString:time];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSData *runData = [[NSData alloc] initWithContentsOfURL:[[NSURL alloc] initFileURLWithPath:[self.runs objectAtIndex:indexPath.row]]];
    NSArray *jsonData = [NSJSONSerialization JSONObjectWithData:runData options:NSJSONReadingAllowFragments error:nil];
    NSMutableArray *coordinatesList = [NSMutableArray new];
    CLLocationCoordinate2D coordinate;
    
    for(NSDictionary *dic in jsonData) {
        coordinate.latitude = [[dic objectForKey:@"lat"] doubleValue];
        coordinate.longitude = [[dic objectForKey:@"lng"] doubleValue];
        
        NSValue *value = [NSValue valueWithBytes:&coordinate objCType:@encode(CLLocationCoordinate2D)];
        [coordinatesList addObject:value];
    }
    
    [self.delegate addItemToVC:self item:coordinatesList];
    [self.navigationController popViewControllerAnimated:YES];
}


@end
