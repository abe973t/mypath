//
//  Model.m
//  MyPath
//
//  Created by Abraham Tesfamariam on 8/7/17.
//  Copyright © 2017 RJT Compuquest. All rights reserved.
//

#import "Model.h"
@import FirebaseAuth;
@import FirebaseDatabase;
@import FirebaseStorage;
@import CoreBluetooth;

@interface NSDictionary(JSONCategories)
+(NSDictionary*)dictionaryWithContentsOfJSONURLString:(NSString*)urlAddress;
-(NSData*)toJSON;
@end

@implementation NSDictionary(JSONCategories)

+ (NSDictionary*)dictionaryWithContentsOfJSONURLString: (NSString*)urlAddress {
    NSData* data = [NSData dataWithContentsOfURL:
                    [NSURL URLWithString: urlAddress] ];
    __autoreleasing NSError* error = nil;
    id result = [NSJSONSerialization JSONObjectWithData:data
                                                options:kNilOptions error:&error];
    if (error != nil) return nil;
    return result;
}

- (NSData*)toJSON {
    NSError* error = nil;
    id result = [NSJSONSerialization dataWithJSONObject:self options:kNilOptions error:&error];
    if (error != nil) return nil;
    return result;
}

@end

@implementation Model

- (instancetype)init {
    if(self = [super init]) {
        self.filehandle = [[NSFileHandle alloc] init];
    }
    
    return self;
}

- (void)startRunLogWithTime:(NSString *)startTime {
    NSString *docPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject];
    NSString *user = [[[FIRAuth auth] currentUser] uid];
    NSString *newDir = [[NSString alloc] initWithFormat:@"%@/%@", docPath, user];
    self.jsonFilePath = [[NSString alloc] initWithFormat:@"%@/%@.json", newDir, startTime];
    NSFileManager *fileManager= [NSFileManager defaultManager];
    
    // checks if directory is already there before it creates it
    if(![fileManager fileExistsAtPath:newDir isDirectory:NULL])
        if(![fileManager createDirectoryAtPath:newDir withIntermediateDirectories:YES attributes:nil error:NULL])
            NSLog(@"Error: Create folder failed %@", newDir);
    
    NSData *jsonFileData = [@"[" dataUsingEncoding:NSUTF8StringEncoding];
    
    // updates json file if it exists, otherwise creates new file
    if ([fileManager fileExistsAtPath:self.jsonFilePath]) {
        self.filehandle = [NSFileHandle fileHandleForUpdatingAtPath:self.jsonFilePath];
    } else if ([fileManager createFileAtPath:self.jsonFilePath contents:jsonFileData attributes:NULL]) {
        self.filehandle = [NSFileHandle fileHandleForWritingAtPath:self.jsonFilePath];
    }

    [self.filehandle seekToEndOfFile];
};

- (void)writeToJSON:(NSMutableDictionary *)path {
    NSData *data = [path toJSON];
    
    [self.filehandle writeData:data];
    NSString *separator = @",";
    [self.filehandle writeData:[separator dataUsingEncoding:NSUTF8StringEncoding]];
    
    [self.filehandle seekToEndOfFile];
}

- (void)stopRun:(NSMutableDictionary *)path {
    NSData *data = [path toJSON];
    NSData *jsonFileData = [@"]" dataUsingEncoding:NSUTF8StringEncoding];
    
    [self.filehandle writeData:data];
    [self.filehandle writeData:jsonFileData];
    
    [self.filehandle seekToEndOfFile];
    [self.filehandle closeFile];
}

- (void)saveRunToFireBase {
    // retreive json file from Caches directory
    NSData *jsonData = [[NSFileManager defaultManager] contentsAtPath:self.jsonFilePath];
    NSString *uid = FIRAuth.auth.currentUser.uid;
    
    // set up Firebase reference variables
    self.ref = [[FIRDatabase database] reference];
    self.storage = [FIRStorage storage];
    FIRStorageReference *storageRef = [self.storage reference];
    NSString *jsonFileName = [[self.jsonFilePath componentsSeparatedByString:@"/"] lastObject];
    FIRStorageReference *jsonRef = [[storageRef child:uid] child:jsonFileName];
    FIRDatabaseReference *runsRef = [[self.ref child:@"runs"] childByAutoId];
    
    // upload json file to Firebase Storage
    FIRStorageUploadTask *uploadTask = [jsonRef putData:jsonData metadata:nil completion:^(FIRStorageMetadata *metadata, NSError *error) {
       if (error != nil) {
           // Uh-oh, an error occurred!
       } else {
           // Metadata contains file metadata such as size, content-type, and download URL.
           NSString *downloadURL = [metadata.downloadURL absoluteString];
           
           [[[[self.ref child:@"runs"] child:runsRef.key] child:@"jsonFile"] setValue:downloadURL];
       }
    }];
    
    // write to paths node
    [[[[self.ref child:@"users"] child:uid] child:@"runs"] updateChildValues:@{runsRef.key:@YES}];
    [[[[self.ref child:@"runs"] child:runsRef.key] child:@"timestamp"] setValue:[jsonFileName stringByDeletingPathExtension]];
    [[[[self.ref child:@"runs"] child:runsRef.key] child:@"user"] setValue:uid];
}

- (NSArray *) loadRuns {
    NSString *docPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject];
    NSString *user = [[[FIRAuth auth] currentUser] uid];
    NSString *newDir = [[NSString alloc] initWithFormat:@"%@/%@", docPath, user];
    NSArray* runsFileName = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:newDir error:NULL];
    NSMutableArray *runs = [[NSMutableArray alloc] init];
    
    for(NSString *str in runsFileName) {
        [runs addObject:[newDir stringByAppendingPathComponent:str]];
    }
    
    __block NSArray *runIDsFromFirebase;
    
    if (runs) {
       return runs;
    } else {
        // load runs from Firebase
        FIRStorageReference *storageRef = [self.storage reference];
        self.ref = [[FIRDatabase database] reference];
        NSString *uid = [FIRAuth auth].currentUser.uid;
        
        FIRDatabaseReference *runsRef = [[[self.ref child:@"users"] child:uid] child:@"runs"];
        [runsRef observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
            NSDictionary *data = [snapshot value];
            runIDsFromFirebase = data.allKeys;
            NSLog(@"%@", [data valueForKey:@"-KrHC7T3GdaVa5YopsQ4"]);
            //data filter
            NSLog(@"These are the runs for %@: \n%@", uid, runIDsFromFirebase);
        }];
    
        runsRef = [self.ref child:@"runs"];
        [runsRef observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
            NSDictionary *runIDs = [snapshot value];
            NSLog(@"This is run data for %@: \n%@", uid, runIDs);
        }];
    }
    
    return NULL;
}

- (void) enableOfflineMode {
    
}

@end
